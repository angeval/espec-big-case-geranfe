package com.itau.bigcase.geranfe.consumers;

import com.itau.bigcase.emissaonfe.models.NotaFiscal;
import com.itau.bigcase.geranfe.services.GeraNfeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Component;

@Component
public class GeraNfeConsumer {

    @Autowired
    private GeraNfeService geraNfeService;

    @KafkaListener(topics = "spec2-angela-valentim-1", groupId = "angela-kafka")
    public void receber(@Payload NotaFiscal notaFiscal) {
        notaFiscal= geraNfeService.calcularImpostos(notaFiscal);
        //empresaService.enviarAoKafka(notaFiscal);
        try {
            geraNfeService.atualizarNotaFiscal(notaFiscal);
          }
        catch (Exception e){
            System.out.println(notaFiscal.getId()+" "+notaFiscal.getValorTotal().doubleValue()+" "+notaFiscal.getIdentidade()
                    +" "+notaFiscal.getStatus()+" "+notaFiscal.getNfe().getValorFinal().doubleValue()+" ");
            System.out.println(e.getStackTrace());
          }
        }


}
