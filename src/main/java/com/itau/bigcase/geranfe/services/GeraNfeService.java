package com.itau.bigcase.geranfe.services;

import com.itau.bigcase.emissaonfe.models.Nfe;
import com.itau.bigcase.emissaonfe.models.NotaFiscal;
import com.itau.bigcase.geranfe.clients.EmpresaClient;
import com.itau.bigcase.geranfe.clients.NotaFiscalClient;
import com.itau.bigcase.geranfe.clients.dtos.EmpresaDTO;
import com.itau.bigcase.geranfe.clients.dtos.NotaFiscalDTO;
import com.itau.bigcase.geranfe.exceptions.EmpresaNotFoundException;
import com.itau.bigcase.geranfe.exceptions.NotaFiscalNotFoundException;
import com.itau.bigcase.geranfe.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class GeraNfeService {
    @Autowired
    private EmpresaClient empresaClient;

    @Autowired
    private NotaFiscalClient notaFiscalClient;

    @Autowired
    private KafkaTemplate<String, NotaFiscal> producer;

    public Boolean validarOptanteSimples(Double capital){
        if (capital > 1000000){
            return true;
        }
        return false;
    }

    public Double buscarCapitalEmpresa(String cnpj) {
        EmpresaDTO empresaDTO = new EmpresaDTO();
        try {
            empresaDTO = empresaClient.getByCnpj(cnpj);
            if (empresaDTO.getStatus() != "ERROR") {
                return empresaDTO.getCapital_social();
            } else {
                throw new EmpresaNotFoundException();
            }
        } catch (Exception e) {
            System.out.println("Consulta da empresa " + cnpj + " apresentou problemas - Erro: " + empresaDTO.getMessage());
        }
        return null;
    }

    public void enviarAoKafka(NotaFiscal notaFiscal) {
        producer.send("spec2-angela-valentim-3", notaFiscal);
    }

    public NotaFiscal calcularImpostos(NotaFiscal notaFiscal) {
        Nfe nfe = new Nfe();
        if (notaFiscal.getIdentidade().length() < 12) {
            //CPF: não tem imposto
            nfe.setValorCofins(new BigDecimal(0));
            nfe.setValorIRRF(new BigDecimal(0));
            nfe.setValorCSLL(new BigDecimal(0));
            nfe.setValorInicial(notaFiscal.getValorTotal());
            nfe.setValorFinal(notaFiscal.getValorTotal());
        } else {
            //CNPJ
            if(validarOptanteSimples(buscarCapitalEmpresa(notaFiscal.getIdentidade()))){
            //Para emitir NF Pessoa Jurídica optante pelo simples nacional. Será descontado somente o IRRF (1,5%) R$ 15,00, valor da NF líquido será R$ 985,00.
                nfe.setValorCofins(new BigDecimal(0));
                double ir = notaFiscal.getValorTotal().doubleValue() * 0.015;
                double valorFinal = notaFiscal.getValorTotal().doubleValue() - ir;
                nfe.setValorIRRF(new BigDecimal(ir));
                nfe.setValorCSLL(new BigDecimal(0));
                nfe.setValorInicial(notaFiscal.getValorTotal());
                nfe.setValorFinal(new BigDecimal(valorFinal));
            }
            else{
            //Para Emitir NF Pessoa Jurídica não optante pelo simples nacional. Será  descontado o IRRF (1,5%) , CSLL (3%) , PIS /Cofins (0,65%). Seria R$ 948,50 valor líquido.
                double ir = notaFiscal.getValorTotal().doubleValue() * 0.015;
                double csll = notaFiscal.getValorTotal().doubleValue() * 0.03;
                double cofins = notaFiscal.getValorTotal().doubleValue() * 0.0065;
                double valorFinal = notaFiscal.getValorTotal().doubleValue() - ir - csll - cofins;
                nfe.setValorCofins(new BigDecimal(cofins));
                nfe.setValorIRRF(new BigDecimal(ir));
                nfe.setValorCSLL(new BigDecimal(csll));
                nfe.setValorInicial(notaFiscal.getValorTotal());
                nfe.setValorFinal(new BigDecimal(valorFinal));
            }
        }
        //nfe.setId(notaFiscal.getId());
        notaFiscal.setNfe(nfe);
        return notaFiscal;
    }

    public void atualizarNotaFiscal(NotaFiscal notaFiscal) throws Exception {
        NotaFiscalDTO notaFiscalDTO = new NotaFiscalDTO();
        try {
            notaFiscalDTO = notaFiscalClient.atualizarNotaFiscal(notaFiscal);
            System.out.println("log: "+notaFiscalDTO.getIdentidade()+" atualizou IR "+notaFiscal.getNfe().getValorIRRF());
        }
        catch (Exception e){
            System.out.println("Não encontrou "+notaFiscal.getId());
            throw new Exception(e);
        }
    }
}
