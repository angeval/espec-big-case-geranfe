package com.itau.bigcase.geranfe.clients;

import com.itau.bigcase.geranfe.exceptions.EmpresaNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class EmpresaClientErrorDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404 || s.contains("ERROR")) {
            return new EmpresaNotFoundException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
