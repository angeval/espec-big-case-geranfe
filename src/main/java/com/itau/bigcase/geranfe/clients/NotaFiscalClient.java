package com.itau.bigcase.geranfe.clients;

import com.itau.bigcase.emissaonfe.models.NotaFiscal;
import com.itau.bigcase.geranfe.clients.dtos.NotaFiscalDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "emissaonfe", configuration = NotaFiscalClientConfiguration.class
            ,url="http://localhost:9000/nfe/atualizar")
public interface NotaFiscalClient {
    @PutMapping
    NotaFiscalDTO atualizarNotaFiscal(@RequestBody NotaFiscal notaFiscal);
}

