package com.itau.bigcase.geranfe.clients;

import com.itau.bigcase.geranfe.clients.dtos.EmpresaDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "buscaEmpresaReceita", configuration = EmpresaClientConfiguration.class
        ,url="https://www.receitaws.com.br")
public interface EmpresaClient {
    @RequestMapping("/v1/cnpj/{cnpj}")
    EmpresaDTO getByCnpj(@PathVariable("cnpj") String cnpj);
}
