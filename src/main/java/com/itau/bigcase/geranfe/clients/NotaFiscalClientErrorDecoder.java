package com.itau.bigcase.geranfe.clients;

import com.itau.bigcase.geranfe.exceptions.NotaFiscalNotFoundException;
import com.itau.bigcase.geranfe.exceptions.UnauthorizedException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class NotaFiscalClientErrorDecoder implements ErrorDecoder {
        private ErrorDecoder errorDecoder = new Default();

        @Override
        public Exception decode(String s, Response response) {
            if (response.status() == 404) {
                return new NotaFiscalNotFoundException();
            } else {
                if (response.status() == 401) {
                    return new UnauthorizedException();
                } else {
                    return errorDecoder.decode(s, response);
                }
            }
        }
}
