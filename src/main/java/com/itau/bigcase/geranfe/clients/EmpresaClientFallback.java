package com.itau.bigcase.geranfe.clients;

import com.itau.bigcase.geranfe.clients.dtos.EmpresaDTO;

public class EmpresaClientFallback implements EmpresaClient {

    private Exception cause;

    EmpresaClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public EmpresaDTO getByCnpj(String cnpj) {

        if (cause.getLocalizedMessage() != null && cause.getLocalizedMessage().contains("EmpresaClientException")) {
            throw new RuntimeException("O serviço de consulta cnpj de empresa está fora do ar");
        }

        throw (RuntimeException) cause;
    }
}
