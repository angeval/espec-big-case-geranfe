package com.itau.bigcase.geranfe.clients;

import com.itau.bigcase.emissaonfe.models.NotaFiscal;
import com.itau.bigcase.geranfe.clients.dtos.NotaFiscalDTO;
import com.itau.bigcase.geranfe.security.Usuario;

public class NotaFiscalClientFallback implements NotaFiscalClient {

    private Exception cause;

    NotaFiscalClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public NotaFiscalDTO atualizarNotaFiscal(NotaFiscal notaFiscal) {
        if (cause.getLocalizedMessage() != null && cause.getLocalizedMessage().contains("NotaFiscalClientException")) {
            throw new RuntimeException("O serviço de nota fiscal está fora do ar");
        }
        //if (cause.getLocalizedMessage().contains("FeignException") || cause.getLocalizedMessage().endsWith("NotFound")) {
        if (cause.getCause().getLocalizedMessage().contains("FeignException")) {
            throw new RuntimeException("O serviço de nota fiscal não encontrou o registro para atualizar");
        }
        throw (RuntimeException) cause;
    }
}