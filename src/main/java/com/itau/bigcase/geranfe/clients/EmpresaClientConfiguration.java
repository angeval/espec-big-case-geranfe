package com.itau.bigcase.geranfe.clients;

import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class EmpresaClientConfiguration {
    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new EmpresaClientErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory(EmpresaClientFallback::new)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}

